<?php
/**
 * @package    Connections
 * @subpackage Template : Button
 * @author     Steven A. Zahm
 * @since      1.0
 * @license    GPL-2.0+
 * @link       https://connections-pro.com
 * @copyright  2013 Steven A. Zahm
 *
 * @wordpress-plugin
 * Plugin Name:       Connections Business Directory Template - Button
 * Plugin URI:        https://connections-pro.com
 * Description:       Button template
 * Version:           1.0
 * Author:            Steven A. Zahm
 * Author URI:        https://connections-pro.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'CN_Button_Template' ) ) {

	/**
	 * Class CN_Button_Template
	 */
	class CN_Button_Template {

		public static function register() {

			$atts = array(
				'class'       => 'CN_Button_Template',
				'name'        => 'Button',
				'slug'        => 'button',
				'type'        => 'all',
				'version'     => '1.0',
				'author'      => 'Steven A. Zahm',
				'authorURL'   => 'connections-pro.com',
				'description' => 'Button template. This template is not recommended for very large directories.',
				'custom'      => FALSE,
				'path'        => plugin_dir_path( __FILE__ ),
				'parts'       => array( 'css' => 'style.css' ),
				);

			cnTemplateFactory::register( $atts );
		}

		/**
		 * @access public
		 * @since  1.0
		 *
		 * @param cnTemplate $template
		 */
		public function __construct( $template ) {

			$this->template = $template;

			$template->part( array( 'tag' => 'card', 'type' => 'action', 'callback' => array( __CLASS__, 'card' ) ) );
			// $template->part( array( 'tag' => 'css', 'type' => 'action', 'callback' => array( $template, 'printCSS' ) ) );
		}

		/**
		 * @access public
		 * @since  1.0
		 *
		 * @param cnOutput $entry
		 */
		public static function card( $entry ) {

            $links = $entry->getLinks(
                array(
                    'type'  => 'page',
                    'limit' => 1,
                )
            );

            $name = $entry->getName();

            if ( 1 <= count( $links ) ) {

                $link = $links[0];

                echo "<a class=\"cn-button\" href=\"{$link->url}\">{$name}</a>";

            } else {

                echo "<a class=\"cn-button\" href=\"#\">{$name}</a>";
            }

		}

	}

	// Register the template.
	add_action( 'cn_register_template', array( 'CN_Button_Template', 'register' ) );
}